import { ActionTypes } from "../contants/action-types"

const initialState = {
    students: [],
};

export const studentReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case ActionTypes.STUDENT_GET_REQUEST:
            return { ...state, students: [] };
        case ActionTypes.STUDENT_GET_SUCCESS:
            return { ...state, students: payload };
        case ActionTypes.STUDENT_GET_FAILURE:
            return state;
        default:
            return state;
    }
};


export const addStudentReducer = (state = {}, action) => {
    switch (action.type) {
        case ActionTypes.STUDENT_ADD_REQUEST:
            return { status: true };
        case ActionTypes.STUDENT_ADD_SUCCESS:
            return {};
        case ActionTypes.STUDENT_ADD_FAILURE:
            return {};
        default:
            return state
    }
}