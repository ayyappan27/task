import { combineReducers } from "redux";
import { studentReducer, addStudentReducer } from "./studentReducer";

const reducers = combineReducers({
    studentReducer,
    addStudentReducer
})

export default reducers;