// export const baseURL = 'http://localhost/project/example-api/Example/';
export const baseURL = 'https://www.appteq.in/react-task/ConTask/';

// get student function
export const getStudent = (student) => {
    // Add student api
    return fetch(baseURL+'getStudentDetails')
    .then((response) => {
        return response.json();
    })
    .catch((err) => {
        return true;
    });
}

// add student function
export const addStudent = (student) => {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(student)
    };        
    // Add student api
    return fetch(baseURL+'AddStudent', requestOptions)
    .then((response) => {
        return response.json();
    })
    .catch((err) => {
        return true;
    });
}