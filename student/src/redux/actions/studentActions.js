import { ActionTypes } from '../contants/action-types';
import { createBrowserHistory } from 'history';
import { getStudent, addStudent } from '../services/student';

export const history = createBrowserHistory();

export const getStudents = () => {
    return dispatch => {    
        dispatch(request());

        getStudent().then(
            (res) => { 
                dispatch(success(res.details));
            },
            (err) => {
                dispatch(failure(err.toString()));
            }
        );
    };

    function request() { return { type: ActionTypes.STUDENT_GET_REQUEST } }
    function success(students) { return { type: ActionTypes.STUDENT_GET_SUCCESS, payload: students } }
    function failure(error) { return { type: ActionTypes.STUDENT_GET_FAILURE, error } }
};

export const addStudents = (student) => {
    return dispatch => {    
        dispatch(request(student));

        addStudent(student).then(
            (res) => { 
                dispatch(success());
                history.goBack();
            },
            (err) => {
                dispatch(failure(err.toString()));
            }
        );
    };

    function request(student) { return { type: ActionTypes.STUDENT_ADD_REQUEST, student } }
    function success(student) { return { type: ActionTypes.STUDENT_ADD_SUCCESS, student } }
    function failure(error) { return { type: ActionTypes.STUDENT_ADD_FAILURE, error } }
}
