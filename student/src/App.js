import React from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import StudentDetails from './component/student/StudentDetails';
import AddStudent from './component/student/AddStudent';
import './App.css';

function App() {
  return (
    <BrowserRouter>
        <Switch>
          <Route exact path="/" component={StudentDetails} />
          <Route exact path="/add-student" component={AddStudent} />
        </Switch>
    </BrowserRouter>
  );
}

export default App;
