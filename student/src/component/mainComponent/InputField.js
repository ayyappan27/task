import React from 'react';

function InputField(props) {
    function handleChange(event){
        if (props.onChange) props.onChange(event)  
    }
    return (
        <input type={props.type} value={props.value} className="cus-input" name={props.name} placeholder={props.placeholder} onChange={handleChange} />
    )
}

export default InputField;


