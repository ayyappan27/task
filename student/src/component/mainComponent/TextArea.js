import React from 'react';

function TextArea(props) {
    function handleChange(event){
        if (props.onChange) props.onChange(event)  
    }
    return (
        <textarea onChange={handleChange} type={props.type} rows="3" value={props.value} className="cus-input" name={props.name} placeholder={props.placeholder}></textarea>
    )
}

export default TextArea;


