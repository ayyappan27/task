import React from 'react';

function SelectInput(props) {
    function handleChange(event){
        if (props.onChange) props.onChange(event)  
    }
    return (
        <select className="cus-input w-27" name={props.name} onChange={handleChange}>
            { props.options.map((data) => (
                <option value={data.value}>{data.name}</option>
            )) }
        </select>
    )
}

export default SelectInput;


