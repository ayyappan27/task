import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import Title from '../mainComponent/Title';
import { useSelector, useDispatch } from 'react-redux';
import { getStudents } from '../../redux/actions/studentActions';

const StudentDetails = (props) => {
    // Use Selectior
    const students = useSelector((state) => state.studentReducer.students);
    // Dispatch
    const dispatch = useDispatch();

    const fetchStudents = () => {
        // Get student details
        dispatch(getStudents());
    }

    useEffect(() => {
        fetchStudents();
    }, []);

    return (
        <div className="cus-div">
            {/* Title Component */}
            <Title title="Students Details" />
            <Link to="/add-student"><p className="cus-add-btn">Add Student</p></Link>
            <table>
                <thead>
                    <tr>
                        <th>S No</th>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Age</th>
                        <th>Standard</th>
                        <th>Skills</th>
                    </tr>
                </thead>
                <tbody>
                    {/* check response value is grater than zero */}
                    { students.length > 0 ?
                        <>
                        { students.map((data, index) => (
                            <tr>
                                <td>{++index}</td>
                                <td>{data.name}</td>
                                <td>{data.gender}</td>
                                <td>{data.age}</td>
                                <td>{data.standard}</td>
                                <td>{data.skills}</td>
                            </tr>
                        ))}
                        </>
                    :
                        <tr>
                            <td colSpan={6}>No Data Available</td>
                        </tr>
                    }
                </tbody>
            </table>
        </div>
    );
}

export default StudentDetails;


