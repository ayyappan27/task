import React, { useState } from 'react';
import Title from '../mainComponent/Title';
import StudentForm from './StudentForm';

function AddStudent() {
    return (
        <div className="cus-div">
            {/* Title Component */}
            <Title title="Add Student" />
            <StudentForm />
        </div>
    );
}

export default AddStudent;


