import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import InputField from '../mainComponent/InputField';
import SelectInput from '../mainComponent/SelectInput';
import TextArea from '../mainComponent/TextArea';
import { useDispatch } from 'react-redux';
import { addStudents } from '../../redux/actions/studentActions';

function StudentForm(props) {
    // Set student state
    const [values, setValues] = useState({
        name: "",
        gender: "",
        age: "",
        standard: "",
        skills: ""
    });
    // Set submit status
    const [submitted, setSubmitted] = useState(false);
    // Dispatch
    const dispatch = useDispatch();

    // Default value of gender
    const genderOptions = [
        { value: "", name: "-- Choose Gender --" },
        { value: "Male", name: "Male" },
        { value: "Female", name: "Female" }
    ];
    
    // set value of state in onchange
    function handleInputChange(e) {
        const { name, value } = e.target;
        setValues(values => ({ ...values, [name]: value }));
    }
    
    // submit function and store
    function handleSubmit(e) {
        e.preventDefault();
        // check input or empty
        if (values.name && values.gender && values.age) {
            dispatch(addStudents(values));
        }
        else{
            // Set submit status true
            setSubmitted(true);
        }
    }

    return (
        <form onSubmit={handleSubmit} className="text-center">
            <div className="cus-form-div">
                <p>Name * :</p>
                <InputField type="text" value={values.name} name="name" placeholder="Enter your name" onChange={handleInputChange} />                    
                { submitted && !values.name ? <p className="error">please enter student name</p> : null }
            </div>
            <div className="cus-form-div">
                <p>Gender * :</p>
                <SelectInput name="gender" onChange={handleInputChange} options={genderOptions} />
                { submitted && !values.gender ? <p className="error">please select gender</p> : null }
            </div>
            <div className="cus-form-div">
                <p>Age * :</p>
                <InputField type="number" value={values.age} name="age" placeholder="Enter your age" onChange={handleInputChange} />
                { submitted && !values.age ? <p className="error">please enter student age</p> : null }
            </div>
            <div className="cus-form-div">
                <p>Standard :</p>
                <InputField type="text" value={values.standard} name="standard" placeholder="Enter your standard" onChange={handleInputChange} />
            </div>
            <div className="cus-form-div">
                <p>Skills :</p>
                <TextArea onChange={handleInputChange} type="text" rows="3" name="skills" className="cus-input" placeholder="Enter your skills" />
            </div>
            <div className="cus-form-div">
                <button className="cus-btn" type="submit">Submit</button>&nbsp;
                <Link to="/"><button className="cus-btn" type="button">Cancel</button></Link>
            </div>
        </form>
    );
}

export default StudentForm;


