<?php  
header('Access-Control-Allow-Origin: *');
   class ExampleMod extends CI_Model  
   {  
        function __construct()  
        {  
            parent::__construct();  
            $this->load->database('default');
        }  

        public function successMessage($type, $data, $message)
        {
            if($type == 'get') {
                $result["status"] = "success";
                $result["statusCode"] = "200";
                $result["message"] = $message;
                $result["details"] = $data;
            }
            else {
                $result["status"] = "success";
                $result["statusCode"] = "200";
                $result["message"] = $message;
            }
            return $result;     
        }
        public function failureMessage($type, $data, $message)
        {
            if($type == 'get') {
                $result["status"] = "fail";
                $result["statusCode"] = "400";
                $result["message"] = $message;
                $result["details"] = $data;
            }
            else {
                $result["status"] = "fail";
                $result["statusCode"] = "400";
                $result["message"] = $message;
            }
            return $result;     
        }


        // ****************************************************************************************
        // Get Student Details
        // ****************************************************************************************

        public function getStudentDetailss()  
        {   
            $this->db->select("*");
            $this->db->from('tbl_student');
            $this->db->where('flag', 1);
            $query = $this->db->get();
            $getResult = $query->num_rows();
            if($getResult == "0" || $getResult == 0) 
            {
                $result = $this->successMessage('get', [], 'No data found');
            }
            else if($getResult > 0 || $getResult > "0"){
                $details = $query->result_array();
                $result = $this->successMessage('get', $details, 'Data found');
            }
            else {
                $result = $this->failureMessage('get', [], 'something went wrong');
            }
            return $result;
        }

        // ****************************************************************************************
        // Add Student
        // ****************************************************************************************
        
        public function AddStudentt($data)  
        {   
            $this->db->insert('tbl_student', $data);
            if ($this->db->affected_rows() == '1')
            {
                $result = $this->successMessage('save', [], 'Student inserted successfully');
            }
            else
            {
                $result = $this->failureMessage('save', [], 'Error in add student');
            }
            return $result;
        }


}

?>