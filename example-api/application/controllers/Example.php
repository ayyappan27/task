<?php
header('Access-Control-Allow-Origin: *');

class Example extends CI_Controller {

    public function __construct() {

        parent:: __construct();
        $this->load->helper('url');
        $this->load->model('ExampleMod');
    }

	/************************* get Country Details Starts Here ************************************/
    
	public function getStudentDetails() 
	{
	    header('Access-Control-Allow-Origin: *');
	    header ("Access-Control-Allow-Headers: *");
	    header('Content-Type: application/json');
	    try {
            $output = $this->ExampleMod->getStudentDetailss();
            echo json_encode($output);
	    }
	    catch (exception $e) {
	        echo $e->getMessage();
	    }       
	}

	public function AddStudent() 
	{
	    header('Access-Control-Allow-Origin: *');
	    header ("Access-Control-Allow-Headers: *");
	    header('Content-Type: application/json');
	    try {
	        $data = json_decode(file_get_contents('php://input'), true);
	        if(isset($data)) {
	            $output = $this->ExampleMod->AddStudentt($data);
	            echo json_encode($output);
	        }
	    }
	    catch (exception $e) {
	        echo $e->getMessage();
	    }       
	}
}

?>